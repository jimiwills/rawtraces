﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Text.RegularExpressions;

using ThermoFisher.CommonCore.Data;
using ThermoFisher.CommonCore.Data.Business;
using ThermoFisher.CommonCore.Data.FilterEnums;
using ThermoFisher.CommonCore.Data.Interfaces;
using ThermoFisher.CommonCore.MassPrecisionEstimator;
using ThermoFisher.CommonCore.RawFileReader;

namespace RawTraces
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename;
            if (args.Length > 0)
            {
                filename = args[0];
            }
            else
            {
                Console.WriteLine("#ERROR: No RAW file specified!");
                return;
            }

            string search = ".*";
            if(args.Length >= 2)
            {
                search = args[1]; 
            }

            List<double> mzsList = new List<double>();
            if (args.Length >= 3)
            {
                if (args[2] != "none")
                {
                    foreach (String mzstring in args[2].Split(','))
                    {
                        mzsList.Add(Convert.ToDouble(mzstring));
                    }
                }
            }
            else
            {
                mzsList.Add(515.33);
                mzsList.Add(523.2854);
                mzsList.Add(421.7583);
                mzsList.Add(737.7062);
            }



            float ppmtol = 10;
            if (args.Length >= 4)
            {
                ppmtol = float.Parse(args[3]);
            }



            double[] mzs = mzsList.ToArray();

            int instrumentCount = checkRawFile(filename);
            if (instrumentCount < 0)
            {
                return;
            }
            if(instrumentCount == 0)
            {
                Console.WriteLine("#no instruments found!");
                return;
            }

            int startScan = -1;
            int endScan = -1;
            //GetChromatogram(filename, startScan, endScan, mzs, ppmtol);
            foreach (double mz in mzs)
            {
                double[] mzsinglet = { mz };
                GetChromatogram(filename, startScan, endScan, mzsinglet, ppmtol);
            }

            if (search != "none")
            {
                // MS
                for (int i = 0; i < instrumentCount; i++)
                {
                    tryChromatograph(filename, i, Device.MS, search);
                }
                // Analog
                for (int i = 0; i < instrumentCount; i++)
                {
                    tryChromatograph(filename, i, Device.Analog, search);
                }
                // UV
                for (int i = 0; i < instrumentCount; i++)
                {
                    tryChromatograph(filename, i, Device.UV, search);
                }
            }


        }
        /*
        private static void ListTrailerExtraFields(IRawDataPlus rawFile)
        {
            // Get the Trailer Extra data fields present in the RAW file
            var trailerFields = rawFile.GetTrailerExtraHeaderInformation();

            // Display each value
            int i = 0;

            Console.WriteLine("Trailer Extra Data Information:");

            foreach (var field in trailerFields)
            {
                Console.WriteLine("   Field {0} = {1} storing data of type {2}", i, field.Label, field.DataType);

                i++;
            }

            Console.WriteLine();
        }
        */

        private static IRawDataPlus openRawFile(string filename)
        {

            if (!File.Exists(filename))
            {
                Console.WriteLine(@"#ERROR: The file doesn't exist in the specified location - " + filename);
                return null;
            }
            IRawDataPlus rawFile = RawFileReaderAdapter.FileFactory(filename);

            if (rawFile.IsError)
            {
                Console.WriteLine("#ERROR: Error trying to read {0} - {1}:{2}", filename, rawFile.FileError.ErrorCode, rawFile.FileError.ErrorMessage);
                return null;
            }
            if (!rawFile.IsOpen)
            {
                Console.WriteLine("#ERROR: Unable to open the RAW file using the RawFileReader class!");
                return null;
            }
            return rawFile;
        }

        private static int checkRawFile(string filename)
        {
            IRawDataPlus rawFile = openRawFile(filename);
            if(rawFile == null)
            {
                return -1;
            }
            if (rawFile.InAcquisition)
            {
                Console.WriteLine("#INFO: RAW file still being acquired - " + filename);
            }
            Console.WriteLine("#INFO: The RAW file has data from {0} instruments", rawFile.InstrumentCount);


            rawFile.SelectInstrument(Device.MS, 1);
            InstrumentData infos = rawFile.GetInstrumentData();
            string[] chanlabs = infos.ChannelLabels;

            Console.WriteLine("#   Instrument model: " + infos.Model);
            Console.WriteLine("#   Instrument name: " + infos.Name);
            Console.WriteLine("#   Serial number: " + infos.SerialNumber);
            Console.WriteLine("#   Software version: " + infos.SoftwareVersion);
            Console.WriteLine("#   Firmware version: " + infos.HardwareVersion);

            /*string[] names = rawFile.GetAllInstrumentFriendlyNamesFromInstrumentMethod();
            Console.WriteLine("#Instrument names:");
            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine("#   {0}",names[i]);
            }
	    if(names.Length == 0){
		    Console.WriteLine("#     no instrument friendly names");
	    }
		*/
            int firstScanNumber = rawFile.RunHeaderEx.FirstSpectrum;
            int lastScanNumber = rawFile.RunHeaderEx.LastSpectrum;

            int count = rawFile.InstrumentCount;
            /*
            KeyValuePair<string, int>[] plottable = rawFile.StatusLogPlottableData;

            Console.Write("plottable length is {0}", plottable.Length);

            var log0 = rawFile.GetStatusLogAtPosition(plottable[0].Value);

            StreamWriter sw = File.CreateText("test.tsv");
            sw.WriteLine("rt\t{0}", plottable[0].Key);
            // Print the chromatogram data (time, intensity values)
            for (int j = 0; j < log0.Times.Length; j++)
            {
                sw.WriteLine("{0:F3}\t{1:F2}", log0.Times[j], log0.Values[j]);
            }
            sw.Close();

            foreach(var pair in plottable)
            {
                Console.WriteLine("plottable name: {0}", pair.Key);
            }
            */

            //ListTrailerExtraFields(rawFile);


            rawFile.Dispose();
            return count;
        }

        private static void tryChromatograph(string filename, int instrument, Device dev, string search)
        {
            // check for positive return from checkRawFile before callin tryChromatogram!
            IRawDataPlus rawFile = openRawFile(filename);
            rawFile.SelectInstrument(dev, instrument);
            if (rawFile.SelectedInstrument.DeviceType == Device.None)
            {
                return;
            }
            //Console.WriteLine("decided on device type {0}", dt);
            InstrumentData info = rawFile.GetInstrumentData();

            ChromatogramTraceSettings settings = new ChromatogramTraceSettings(TraceType.A2DChannel1);
            // ChromatogramTraceSettings settings = new ChromatogramTraceSettings(TraceType.TotalAbsorbance);

            IChromatogramData data = rawFile.GetChromatogramData(new IChromatogramSettings[] { settings }, -1, -1);
            ChromatogramSignal[] trace = ChromatogramSignal.FromChromatogramData(data);
            if (trace[0].Length > 0)
            {
                int iChannel = 0;
                while (iChannel == 0 || iChannel < info.ChannelLabels.Length)
                {
                    string channel = "nolabel";
                    if (iChannel < info.ChannelLabels.Length)
                    {
                        channel = info.ChannelLabels[iChannel];
                    }
                    string fileoutname = filename + "trace" +
                        rawFile.SelectedInstrument.InstrumentIndex + rawFile.SelectedInstrument.DeviceType
                        + "_" + iChannel + channel + ".tsv";

                    if(Regex.Match(fileoutname, search, RegexOptions.IgnoreCase).Success)
                    {
                        StreamWriter f = File.CreateText(fileoutname);
                        Console.WriteLine("{0}", fileoutname);
                        f.WriteLine("rt\t{0}", channel);
                        // Print the chromatogram data (time, intensity values)
                        for (int j = 0; j < trace[0].Length; j++)
                        {
                            f.WriteLine("{0:F3}\t{1:F0}", trace[0].Times[j], trace[0].Intensities[j]);
                        }
                        f.Close();
                    }

                    iChannel++;
                }
            }

        }

        private static void GetChromatogram(string filename, int startScan, int endScan, double[] mzs, float ppmtol)
        {

            IRawDataPlus rawFile = openRawFile(filename);
            rawFile.SelectInstrument(Device.MS, 1);
            // Define the settings for getting the Base Peak chromatogram
            //ChromatogramTraceSettings settings = new ChromatogramTraceSettings(TraceType.MassRange);
            ThermoFisher.CommonCore.Data.Business.Range[] ranges = new Range[mzs.Length];
            int i = 0;
            foreach (double mz in mzs)
            {
                double deltamz = mz * ppmtol / 1000000;
                double low = mz - deltamz;
                double high = mz + deltamz;

                Console.WriteLine("# {0} x {1} => {2}, {3}",mz, ppmtol, low, high);
                ranges.SetValue(new ThermoFisher.CommonCore.Data.Business.Range(low, high), i);
                i++;
            }

            ChromatogramTraceSettings settings = new ChromatogramTraceSettings("MS", ranges);
            ThermoFisher.CommonCore.Data.Business.Range range3 = settings.GetMassRange(0);
            Console.WriteLine("# {0} {1} {2}", range3.Low, range3.High, range3.GetType());


            // Get the chromatogram from the RAW file. 
            IChromatogramData data = rawFile.GetChromatogramData(new IChromatogramSettings[] { settings }, startScan, endScan);

            // Split the data into the chromatograms
            ChromatogramSignal[] trace = ChromatogramSignal.FromChromatogramData(data);
            
            if (trace[0].Length > 0)
            {
                string channel = "mz" + string.Join("-", mzs.Select(x => x.ToString()).ToArray());
                string fileoutname = filename + "trace" + channel + ".tsv";
                // Print the chromatogram data (time, intensity values)
                Console.WriteLine("# Base Peak chromatogram ({0} points)", trace[0].Length);



                StreamWriter f = File.CreateText(fileoutname);
                Console.WriteLine("{0}", fileoutname);
                f.WriteLine("rt\t{0}", channel);
                // Print the chromatogram data (time, intensity values)
                for (int j = 0; j < trace[0].Length; j++)
                {
                    f.WriteLine("{0:F3}\t{1:F0}", trace[0].Times[j], trace[0].Intensities[j]);
                }
                f.Close();
            }
            /*
            if (trace[0].Length > 0)
            {
                string channel = "allMzRanges";
                string fileoutname = filename + "trace" + channel + ".tsv";
                // Print the chromatogram data (time, intensity values)
                Console.WriteLine("Base Peak chromatogram ({0} points)", trace[0].Length);



                StreamWriter f = File.CreateText(fileoutname);
                Console.WriteLine("{0}", fileoutname);
                f.Write("rt");
                foreach(var mz in mzs)
                {
                    f.Write("\tmz{0}", mz);
                }
                f.WriteLine("");
                // Print the chromatogram data (time, intensity values)
                for (int j = 0; j < trace[0].Length; j++)
                {
                    f.Write("{0:F3}", trace[0].Times[j]);
                    for(i=0; i<mzs.Length; i++) { 
                        f.Write("\t{0:F0}", trace[i].Intensities[j]);
                    }
                    f.WriteLine("");
                }
                f.Close();
            }
            i++;
            */
        }
    }
}
