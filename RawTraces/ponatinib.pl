#!perl

use strict;
use warnings;

my %traces = ();

my $timeBinWidth = 0.1;

foreach my $raw(map {glob $_} @ARGV){
	next unless $raw =~ /\.raw$/i;
	my @lines = split /[\n\r]+/, `~/git/igmmms-sftp/bin/RawTraces "$raw" "none" "533.2268,267.1169,519.2112,260.1091,549.2218,275.1144,418.2214,426.1381,565.2167,283.1119"`;
	my @tsvs = grep ! /^#/, @lines;
	my @pngs = ();
	foreach my $tsv(@tsvs){
		print "$tsv\n";
		my $rawlength = length($raw);
		die "something went wrong $tsv <> $raw " unless substr($tsv, 0, $rawlength) eq $raw;
		my $trace = substr($tsv, $rawlength);
		print "Tracename: $trace\n";
		if($trace =~ /^tracemz(\d+\.\d+)\.tsv$/){
			my $mz = "mz$1";
			print ": $mz\n";
			$traces{$mz} = {'.Max'=>{}} unless exists $traces{$mz};
			my %ts = readTimeSeries($tsv);
			$traces{$mz}->{'.Max'} = {maxTimeSeries($traces{$mz}->{'.Max'}, \%ts)};
			$traces{$mz}->{$raw} = \%ts;
		}

		# if($tsv =~ /pressure|flow.*RightBlk|1MS_0/i){
		# 	print "png...\n";
		# 	my $png = `Rscript ~/git/igmmms-sftp/plot.R --xmin 1 "$tsv"`;
		# 	push @pngs, $png;
		# }
		# unlink($tsv);
	}
}

foreach my $mz(keys %traces){
	my $table = $traces{$mz};
	print "outputting collated chromatograms for $mz\n";
	my @raws = sort keys %$table;
	my @times = sort {$a <=> $b} keys %{$table->{'.Max'}};
	open(my $fh, '>', "$mz.tsv") or die "Could not write $mz.tsv: $!";
	print $fh join("\t", "time", @raws), "\n";
	foreach my $t(@times){
		print $fh $t;
		foreach my $r(@raws){
			print $fh "\t" . (exists $table->{$r}->{$t} ? $table->{$r}->{$t} : 0);
		}
		print $fh "\n";
	}
	close($fh);
}

sub maxTimeSeries {
	my ($ts1, $ts2) = @_;
	my %ts = ();
	$ts{$_} = 0 foreach keys(%$ts1),keys(%$ts2);
	foreach (keys %ts){
		# at least one of them must exist!
		if(! exists $ts1->{$_}){
			$ts{$_} = $ts2->{$_};
		}
		elsif(! exists $ts2->{$_}){
			$ts{$_} = $ts1->{$_};
		}
		else {
			# if neither doesn't exists, they must both exist
			# pick the max
			$ts{$_} = $ts1->{$_} > $ts2->{$_} ? $ts1->{$_} : $ts2->{$_};
		}
	}
	return %ts;
}

sub readTimeSeries {
	my ($fn) = @_;
	open(my $fh, '<', $fn) or die "could not read $fn: $_";
	my %ts = ();
	while(<$fh>){
		next unless /([\d.]+)\s+([\d.]+)/;
		my ($time,$value) = ($1,$2);
		$time = $timeBinWidth * int($time / $timeBinWidth);
		$ts{$time} = 0 unless exists $ts{$time};
		$ts{$time} += $value;
	}
	close($fh);
	return %ts;
}

